var fs = require('fs');
var express = require('express');
var path = require('path');
var routeElektronik = require('./app_server/routes/ElektronikRoute');
var ejsLayout = require('express-ejs-layouts');

var app = express();

app.set('view engine', 'ejs');
app.set('views',path.join(__dirname, '/app_server/views'));

app.use(ejsLayout);
app.use('/public', express.static(path.join(__dirname, 'public')));

app.use('/elektronik', routeElektronik);

app.listen(8000);