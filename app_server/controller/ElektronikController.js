var path = require('path');

module.exports.index = function (req, res) {
    res.render('elektronik');
};

module.exports.bilgisayar = function (req, res) {

    var computerBrands = ['Dell', 'Sony', 'Apple', 'Samsung'];

    res.render('bilgisayar', {computerBrands:computerBrands});
};


